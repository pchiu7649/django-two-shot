from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account

from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
# Receipts list view
@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipt_list": receipts,
    }

    return render(request, "receipts/home.html", context)


# ExpenseCategory list view
@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "expense_list": expense,
    }
    return render(request, "categories/list.html", context)


# Account list view
@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {"account_list": accounts}
    return render(request, "accounts/list.html", context)


# Create receipt
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# Create Category
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# Create Account
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
